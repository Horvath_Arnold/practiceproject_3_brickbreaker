
# PracticeProject_3_BrickBreaker
<br />

        Functionalities
- hit all the bricks with the ball, using the pallet
- left, right arrow keys move the paddle
- the ball bounces back, once a brick, or the side of the screen is hit
- dont let the ball fall down
- sound effects added
- delta missing - learned the hard way

![Logo](gameplayPic.png)

<br />

        Clone the project (in bash) 
- git clone https://gitlab.com/Horvath_Arnold/practiceproject_3_brickbreaker.git
- cd practiceproject_3_brickbreaker

<br />


	    How to run with cmd (in bash) 
- you need a Java JDK or JRE installed and set up on your machine if you want to run these projects. Clone the project and simply import it after.
- cd practiceproject_3_brickbreaker
- java -jar snake.jar

<br />

        How to run in Eclipse
- open the project in Eclipse
- run from App.main()