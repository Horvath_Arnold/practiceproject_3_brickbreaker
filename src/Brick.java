
public class Brick{
	private final int xCoord;
	private final int yCoord;
	
	public Brick(int xCoord, int yCoord) {
		this.xCoord = xCoord;
		this.yCoord = yCoord;
	}

	public int getXCoord() {
		return xCoord;
	}

	public int getYCoord() {
		return yCoord;
	}
	
}
