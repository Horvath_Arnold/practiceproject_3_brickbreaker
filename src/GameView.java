import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JPanel;

public class GameView extends JPanel{
	private final int width; 							// size of GameView JPanel
	private final int height;
	private final int borderThickness = 5; 				// borderThickness of side borders that the ball jumps back from
	private GameModel gameModel;
	public Ball ball;
	private Paddle paddle;
	
	public GameView(int width, int height, GameModel gameModel) {
		this.width = width;
		this.height = height;
		setBackground(Color.BLACK);
		setPreferredSize(new Dimension(width, height));
		setFocusable(true);
		requestFocusInWindow();
		this.gameModel = gameModel;
		this.ball = gameModel.getBall();
		this.paddle = gameModel.getPaddle();
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if(gameModel.isGameOver()) {
			paintGameOver(g);
		} else {
			paintGame(g);
		}
		g.dispose();
	}
	
	private void paintGameOver(Graphics g) {
		String gameOverMessage;
		if(gameModel.areAllBricksHit())
			gameOverMessage = "YOU WON! Score reached:";
		else
			gameOverMessage = "Game over! Score reached:";
		
		g.setColor(Color.RED);
		g.setFont(new Font("serif", Font.BOLD, 30));
		g.drawString(gameOverMessage + gameModel.getScore(), 190, 300);
		g.drawString("Press SPACE to restart the game!", 190, 350);
	}
	
	private void paintGame(Graphics g) {
		// paddle
		g.setColor(Color.GREEN);
		g.fillRect(paddle.getXCoord(), paddle.getYCoord(), paddle.getWidth(), paddle.getHeight());
		
		// ball
		g.setColor(Color.BLUE);
		g.drawOval(ball.getXCoord(), ball.getYCoord(), ball.getSize(), ball.getSize());
		g.setColor(Color.RED);
		g.fillOval(ball.getXCoord(), ball.getYCoord(), ball.getSize() - 1, ball.getSize() - 1);
		
		// add the borders to the panel, on sides UP, LEFT, RIGHT
		g.setColor(Color.RED);
		g.fillRect(0, 0, width, borderThickness); 							// up
		g.fillRect(0, 0, borderThickness, height); 							// left
		g.fillRect(width - borderThickness, 0, borderThickness, height); 	// right
		
		// bricks
		int brickWidth = gameModel.getBrickWidth();
		int brickHeight = gameModel.getBrickHeight();
		for(int i = 0; i < gameModel.getRowOfBricks(); i++) {
			int currentBrickYCoord = gameModel.getBricks(i, 0).getYCoord();
			for(int j = 0; j < gameModel.getColOfBricks(); j++) {
				if(gameModel.isActiveBrick(i, j)) { 				// only paint a brick, if it wasn't hit
					int currentBrickXCoord = gameModel.getBricks(i, j).getXCoord();
					g.setColor(Color.RED);
					g.drawRect(currentBrickXCoord, currentBrickYCoord, brickWidth, brickHeight);
					g.setColor(Color.YELLOW);
					g.fillRect(currentBrickXCoord, currentBrickYCoord, brickWidth - 1, brickHeight - 1);
				}
			}
		}
	}
	
	public int getBorderThickness() {
		return borderThickness;
	}
	
}
