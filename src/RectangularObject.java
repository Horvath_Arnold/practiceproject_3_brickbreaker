
public class RectangularObject {
	protected int width;
	protected int height;
	protected int xCoord;
	protected int yCoord;
	
	public int getWidth() { return width; }

	public int getHeight() { return height; }

	public int getXCoord() { return xCoord; }

	public int getYCoord() { return yCoord; }

	public void setXCoord(int xCoord) {
		this.xCoord = xCoord;
	}
	
	public void setYCoord(int yCoord) {
		this.yCoord = yCoord;
	}
}
