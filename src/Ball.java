
public class Ball extends RectangularObject{
	private final int size;
	private int directionXCoord; 	// ball move direction modifiers, ball starts by going N-W direction
	private int directionYCoord;
	
	public Ball(int x, int y, int s) {
		size = s;
		setBallToStartingPosition(x, y);
	}
	
	public int getSize() { return size; }
	
	public int getDirectionXCoord() { return directionXCoord; }
	
	public void setDirectionXCoord(int newDirX) { directionXCoord = newDirX; }
	
	public int getDirectionYCoord() { return directionYCoord; }
	
	public void setDirectionYCoord(int newDirY) { directionYCoord = newDirY; }
	
	public void rollBall() {
		xCoord += directionXCoord;
		yCoord += directionYCoord;
	}
	
	public void setBallToStartingPosition(int x, int y) {
		directionXCoord = 0;
		directionYCoord = 1;
		xCoord = x;
		yCoord = y;
		
	}
}
