import java.awt.Dimension;

public class GameModel {
	private boolean[][] currentlyActiveBricks; 		// bricks that are still "alive"
	private final int rowOfBricks = 4;
	private final int colOfBricks = 5;
	private final int brickWidth;
	private final int brickHeight;
	private Brick[][] bricks;
	private int score;
	private Ball ball;
	private Paddle paddle;
	private int frameWidth;
	private int frameHeight;
	private boolean gameIsOver;
	
	public GameModel(Dimension brickDimension, int frameWidth, int frameHeight) {
		brickWidth = (int) brickDimension.getWidth();
		brickHeight = (int) brickDimension.getHeight();
		this.frameHeight = frameHeight;
		this.frameWidth = frameWidth;
		score = 0;
		gameIsOver = false;
		ball = new Ball(frameWidth / 2 - 10, frameHeight - 100, 20);
		paddle = new Paddle(frameWidth / 2 - 50, frameHeight - 40, 100, 10, frameWidth);
		createBricks();
	}
	
	public void resetGameModel() {
		ball.setBallToStartingPosition(frameWidth / 2 - 10, frameHeight - 100);
		paddle.setPaddleToStartingPosition(frameWidth / 2 - 50, frameHeight - 40);
		score = 0;
		gameIsOver = false;
		createBricks();
	}
	
	private void createBricks() {
		bricks = new Brick[rowOfBricks][colOfBricks];
		currentlyActiveBricks = new boolean[rowOfBricks][colOfBricks];
		for(int i = 0; i < rowOfBricks; i++) {
			int currentBrickYCoord = i * (brickHeight + 5) + 50;
			for(int j = 0; j < colOfBricks; j++) {
				int currentBrickXCoord = j * (brickWidth + 5) + 100;
				Brick currentBrick = new Brick(currentBrickXCoord, currentBrickYCoord);
				bricks[i][j] = currentBrick;
				currentlyActiveBricks[i][j] = true;
			}
		}
	}

	public boolean isActiveBrick(int i, int j) {
		return currentlyActiveBricks[i][j];
	}
	
	public void setActiveBrick(int i, int j) {
		currentlyActiveBricks[i][j] = false;
	}

	public int getRowOfBricks() {
		return rowOfBricks;
	}

	public int getColOfBricks() {
		return colOfBricks;
	}

	public int getBrickWidth() {
		return brickWidth;
	}

	public int getBrickHeight() {
		return brickHeight;
	}

	public Brick getBricks(int i, int j) {
		return bricks[i][j];
	}
	
	public void incrementScore() {
		score++;
	}
	
	public int getScore() {
		return score;
	}
	
	public boolean isGameOver() {
		return gameIsOver;
	}
	
	public void setGameToOver() {
		gameIsOver = true;
	}
	
	public boolean ballFellOutOfLevel() {
		if(ball.getYCoord() > frameHeight) {
			gameIsOver = true;
			return true;
		}
		return false;
	}
	
	public boolean areAllBricksHit() {
		if(score == rowOfBricks * colOfBricks) {
			gameIsOver = true;
			return true;
		}
		return false;
	}
	
	public Ball getBall() {
		return ball;
	}
	
	public Paddle getPaddle() {
		return paddle;
	}

}
