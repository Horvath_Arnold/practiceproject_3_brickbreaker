import java.awt.Dimension;
import java.awt.Rectangle;
import javax.swing.JFrame;

public class GameFrame extends JFrame{
	private GameView gameView;
	private final int gameFrameWidth = 800;
	private final int gameFrameHeight = 550;
	private final Dimension brickDimension = new Dimension(115, 50); 		// these are the sizes of a brick
	private GameModel gameModel;
	
	public GameFrame() {
		setTitle("Brick Breaker");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(new Rectangle(gameFrameWidth,gameFrameHeight));
		setResizable(false);
		setLocationRelativeTo(null);
		
		gameModel = new GameModel(brickDimension, gameFrameWidth, gameFrameHeight);
		gameView = new GameView(gameFrameWidth, gameFrameHeight, gameModel);
		new GameControl(gameView, gameModel);
		setContentPane(gameView);
		pack();
		
		setVisible(true);
	}
}