import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import javax.swing.Timer;

public class GameControl implements ActionListener{
	private GameView gameView;
	private GameModel gameModel;
	private Ball ball;
	private Paddle paddle;
	private Timer timer;
	private int delay = 5;
	
	public GameControl(GameView gameView, GameModel gameModel) {
		this.gameView = gameView;
		this.gameModel = gameModel;
		ball = gameModel.getBall();
		paddle = gameModel.getPaddle();
		createKeyBindings();
		timer = new Timer(delay, this); 		// runs down every 0.005 sec. Paints and moves ball
		timer.start();
	}
	
	private void createKeyBindings() {
		Action actionMoveLeft = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				paddle.movePaddleLeft();
			}
		};
		addGameViewKeyListener(actionMoveLeft, KeyEvent.VK_LEFT, "moveLeft");
		
		Action actionMoveRight = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				paddle.movePaddleRight();
			}
		};
		addGameViewKeyListener(actionMoveRight, KeyEvent.VK_RIGHT, "moveRight");
		
		Action actionRestartGame = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				gameModel.resetGameModel();
				timer.start(); 		// timer starts again, once model is reset and SPACE is pushed
			}
		};
		addGameViewKeyListener(actionRestartGame, KeyEvent.VK_SPACE, "restartGame");
	}
	
	private void addGameViewKeyListener(Action a, int keyCode, String actionName) {	// KeyListener on SPACE, LEFT, RIGHT
		// when window that component is in is focused, generate space action
		gameView.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(keyCode, 0), actionName);
		gameView.getActionMap().put(actionName, a);
	}
	
	// this action is performed every time the timer ticks (0.005 sec)
	@Override
	public void actionPerformed(ActionEvent e) {
		if(gameModel.areAllBricksHit() || (!gameModel.ballFellOutOfLevel() && !gameModel.isGameOver())){
			checkBallToBordersIntersection();	// check if ball hits the up, left, right borders
			checkBallToPaddleIntersection(); 	// check if ball hits paddle
			checkBallToBrickIntersection(); 	// check if ball hits brick
			ball.rollBall();
			gameView.repaint();
		}
		
		if(gameModel.isGameOver()) { 		// if the game is over, paint the GameOver and score and stop timer.
			gameView.repaint();
			if(gameModel.areAllBricksHit())
				new SoundPlayer("Sounds//gameWon.wav").start();
			else
				new SoundPlayer("Sounds//gameLost.wav").start();
			timer.stop();
		}
	}
	
	
	private void playSound(String fileName) {
		new SoundPlayer(fileName).start();
	}
	
	
	// method that redirects the ball, if the ball rectangle (made out of ball data) intersects with any border rectangle
	private void checkBallToBordersIntersection() {
		Rectangle ballRect = new Rectangle(ball.getXCoord(), ball.getYCoord(), ball.getSize(), ball.getSize());
		boolean borderHit = false;
		
		Rectangle leftBorderRect = new Rectangle(0, 0, gameView.getBorderThickness(), gameView.getHeight());
		if(ballRect.intersects(leftBorderRect)) {
			ball.setDirectionXCoord(ball.getDirectionXCoord() * (-1));
			borderHit = true;
		}
		
		Rectangle upperBorderRect = new Rectangle(0, 0, gameView.getWidth(), gameView.getBorderThickness());
		if(ballRect.intersects(upperBorderRect)) {
			ball.setDirectionYCoord(ball.getDirectionYCoord() * (-1));
			borderHit = true;
		}

		Rectangle rightBorderRect = new Rectangle(gameView.getWidth() - gameView.getBorderThickness(), 0, gameView.getBorderThickness(), gameView.getHeight());
		if(ballRect.intersects(rightBorderRect)) {
			ball.setDirectionXCoord(ball.getDirectionXCoord() * (-1));
			borderHit = true;
		}
		if(borderHit)
			playSound("Sounds//ballToBorder.wav");
	}
	
	
	private void checkBallToPaddleIntersection() {
		Rectangle ballRect = new Rectangle(ball.getXCoord(), ball.getYCoord(), ball.getSize(), ball.getSize());
		Rectangle paddleRect = new Rectangle(paddle.getXCoord(), paddle.getYCoord(), paddle.getWidth(), paddle.getHeight());
		if(ballRect.intersects(paddleRect)) {
			checkWhereBallHitPaddle(ball, paddle);
			rebounceBallInCorrectDirection(ballRect, paddleRect, ball.getSize());
			playSound("Sounds//ballToPaddle.wav");
		}
	}
	
	
	/*
	 * Cut the paddle into 3 equal parts. Width is 100, so center part starts from XCoord = 33, 
	 * right part starts from XCoord = 66;
	 * When ball hits left side of the paddle, direction is changed to LEFT for the ball
	 * When ball hits right side of the paddle, direction is changed to RIGHT for the ball
	 */
	private void checkWhereBallHitPaddle(Ball ball, Paddle paddle) {
		int ballCenter = (ball.getXCoord() + ball.getSize() / 2);
		int paddleMiddleXCoord = paddle.getXCoord() + paddle.getWidth() / 3;
		int paddleRightXCoord = paddle.getXCoord() + paddle.getWidth() * 2/ 3;
		if(ballCenter >= paddleRightXCoord) {
			ball.setDirectionXCoord(1);
			ball.setXCoord(ball.getXCoord() + 1);
		} else if( ballCenter >= paddleMiddleXCoord) {
			ball.setDirectionXCoord(0);
		} else {
			ball.setDirectionXCoord(-1);
			ball.setXCoord(ball.getXCoord() - 1);
		}
	}
	
	
	// method that deals with ball hitting rectangles (brick or paddle) and changing the balls direction
	private void rebounceBallInCorrectDirection(Rectangle ballRect, Rectangle otherRect, int ballSize) {
		// if(ball hits brick/paddle from LEFT side of brick/paddle || 
		// ball hits brick/paddle from RIGHT side of brick, the X coord direction is changed
		if((ballRect.x + ballSize - 1 <= otherRect.x) || (ballRect.x + 1 >= otherRect.x + otherRect.width))
			ball.setDirectionXCoord(ball.getDirectionXCoord() * (-1));
		 else 		// else if the ball hits the brick from UP or DOWN sides, the Y coord direction is changed
			ball.setDirectionYCoord(ball.getDirectionYCoord() * (-1));
	}
	
	
	private void checkBallToBrickIntersection() {
		Rectangle ballRect = new Rectangle(ball.getXCoord(), ball.getYCoord(), ball.getSize(), ball.getSize());
		boolean foundHit = false; 			// if the hit was registered, exit the loops
		for(int i = 0; i < gameModel.getRowOfBricks() && !foundHit; i++) {
			for(int j = 0; j < gameModel.getColOfBricks() && !foundHit; j++) {
				if(gameModel.isActiveBrick(i,j)) {
					Rectangle brickRect = new Rectangle(gameModel.getBricks(i, j).getXCoord(), 
							gameModel.getBricks(i, j).getYCoord(),
							gameModel.getBrickWidth(),
							gameModel.getBrickHeight());
					
					if(ballRect.intersects(brickRect)) {
						playSound("Sounds//ballToBrick.wav");
						gameModel.incrementScore();
						gameModel.setActiveBrick(i, j);
						rebounceBallInCorrectDirection(ballRect, brickRect, ball.getSize());
						foundHit = true; 
						gameView.repaint();
					}
				}
			}
		}
	}
}
