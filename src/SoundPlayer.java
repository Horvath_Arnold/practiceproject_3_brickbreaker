import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class SoundPlayer extends Thread{

	private String fileName;
	
	public SoundPlayer(String file) {
		fileName = file;
	}
	
	public void run() {
		try {
			// create AudioInputStream object 
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(fileName));
			Clip clip = AudioSystem.getClip(); 		// initialize clip (get clip from AudioSystem)
	        clip.open(audioInputStream);  			// load the sound we want to play into clip
	        clip.start();
	        try {
				Thread.sleep(clip.getMicrosecondLength() / 1000); 		// sleep for the length of the clip, then dies out
			} catch (InterruptedException e) {
				System.out.println("Error! Sleeping soundthread! " + e);
			}
		} catch (UnsupportedAudioFileException e) {
			System.out.println("Error! Unsupported Audio file! " + e);
		} catch (IOException e) {
			System.out.println("Error! IOException! " + e);
		} catch (LineUnavailableException e) {
			System.out.println("Error! LineUnavailableException! " + e);
		}
	}
}
