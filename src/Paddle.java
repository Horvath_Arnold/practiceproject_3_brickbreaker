
public class Paddle extends RectangularObject {
	private final int moveSidewaysValue;
	private final int screenWidth;
	
	public Paddle(int xCoord, int yCoord, int w, int h, int screenWidth) {
		width = w;
		height = h;
		moveSidewaysValue = 20;
		this.screenWidth = screenWidth;
		setPaddleToStartingPosition(xCoord, yCoord);
	}

	// move the paddle left, by -- modifying the x coordinate
	public void movePaddleLeft() {
		xCoord -= moveSidewaysValue;
		if(xCoord <= 0)
			xCoord = 0;
	}
	
	// move the paddle right, by ++ modifying the x coordinate
	public void movePaddleRight() {
		xCoord += moveSidewaysValue;
		if(xCoord >= screenWidth - width)
			xCoord = screenWidth - width;
	}
	
	public void setPaddleToStartingPosition(int x, int y) {
		xCoord = x;
		yCoord = y;
	}
}
